package net.sf.okapi.lib.search.lucene.scorer;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import java.util.Set;

import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.lib.search.lucene.analysis.NgramAnalyzer;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

public class Util {
	/**
	 * Calculate Dice's Coefficient
	 * 
	 * @param intersection
	 *            number of tokens in common between input 1 and input 2
	 * @param size1
	 *            token size of first input
	 * @param size2
	 *            token size of second input
	 * @return Dice's Coefficient as a float
	 */
	public static float calculateDiceCoefficient(int intersection, int size1, int size2) {
		return (float) ((2.0f * (float) intersection)) / (float) (size1 + size2) * 100.0f;
	}

	/**
	 * Calculate Dice's Coefficient for two strings with tokens as ngrams.
	 * 
	 * @param originalSource
	 *            first string to compare
	 * @param newSource
	 *            second string to compare
	 * @param triGramAnalyzer
	 *            trigram analyzer for FM-score calculation
	 * @return Dice's Coefficient as a float
	 */
	public static float calculateNgramDiceCoefficient(String originalSource, String newSource,
					Analyzer triGramAnalyzer) {
		Set<String> originalSourceTokens = new HashSet<>();
		Set<String> newSourceTokens = new HashSet<>();

		// get old source string tokens
		try (TokenStream stream = triGramAnalyzer.tokenStream("", originalSource)) { //TokenStream is AutoCloeable
			CharTermAttribute termAtt = stream.addAttribute(CharTermAttribute.class);
			stream.reset();
			while (stream.incrementToken()) {
				originalSourceTokens.add(termAtt.toString());
			}
			stream.end();
		} catch (IOException e) {
			throw new OkapiException("Error tokenizing source TextUnits", e);
		}

		try (TokenStream stream = triGramAnalyzer.tokenStream("", newSource)) { //TokenStream is AutoCloeable
			CharTermAttribute termAtt = stream.addAttribute(CharTermAttribute.class);
			stream.reset();
			while (stream.incrementToken()) {
				newSourceTokens.add(termAtt.toString());
			}
			stream.end();
		} catch (IOException e) {
			throw new OkapiException("Error tokenizing source TextUnits", e);
		}

		// now calculate dice coefficient to get fuzzy score
		int originalSize = originalSourceTokens.size();
		int newSize = newSourceTokens.size();
		originalSourceTokens.retainAll(newSourceTokens);
		int intersection = originalSourceTokens.size();
		return (float) ((2.0f * (float) intersection)) / (float) (originalSize + newSize) * 100.0f;
	}

}
