/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

final class SharedStrings {
	private static final int START_INDEX = 0;
	private int nextIndex = START_INDEX;
	private final ArrayList<Item> items;

	SharedStrings() {
		this.items = new ArrayList<>();
	}

	int nextIndex() {
		return this.nextIndex;
	}

	void add(final Item item) {
		this.items.ensureCapacity(item.newIndex() + 1);
		this.items.add(item.newIndex(), item);
		this.nextIndex++;
	}

	List<Item> items() {
		return items;
	}

	boolean worksheetStartsAt(final int index) {
		if (START_INDEX == index) {
			return true;
		}
		return !this.items.get(index - 1).worksheet().equals(this.items.get(index).worksheet());
	}

	String worksheetAt(int index) {
		return items.get(index).worksheet();
	}

	CellReferencesRange cellReferencesRangeAt(final int index) {
		return this.items.get(index).cellReferencesRange();
	}

	boolean rowStartsAt(final int index) {
		return this.items.get(index - 1).cellReferencesRange().first().row() != this.items.get(index).cellReferencesRange().first().row();
	}

	boolean visibleAt(int index) {
		return !items.get(index).excluded();
	}

	MetadataContext metadataContextAt(final int index) {
		return this.items.get(index).metadataContext();
	}

	static final class Item {
		private final int origIndex;
		private final int newIndex;
		private final List<XMLEvent> events;
		private final String worksheet;
		private final CellReferencesRange cellReferencesRange;
		private final boolean excluded;
		private final MetadataContext metadataContext;

		Item(
			final int origIndex,
			final int newIndex,
			final List<XMLEvent> events,
			final String worksheet,
			final CellReferencesRange cellReferencesRange,
			final boolean excluded,
			final MetadataContext metadataContext
		) {
			this.origIndex = origIndex;
			this.newIndex = newIndex;
			this.events = events;
			this.worksheet = worksheet;
			this.cellReferencesRange = cellReferencesRange;
			this.excluded = excluded;
			this.metadataContext = metadataContext;
		}

		int originalIndex() {
			return origIndex;
		}

		int newIndex() {
			return newIndex;
		}

		List<XMLEvent> events() {
			return this.events;
		}

		String worksheet() { return worksheet; }

		CellReferencesRange cellReferencesRange() {
			return this.cellReferencesRange;
		}

		boolean excluded() {
			return excluded;
		}

		MetadataContext metadataContext() {
			return this.metadataContext;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || !(o instanceof Item)) return false;
			Item e = (Item)o;
			return origIndex == e.origIndex && newIndex == e.newIndex;
		}
		@Override
		public int hashCode() {
			return Objects.hash(origIndex, newIndex);
		}

		@Override
		public String toString() {
			return "Item("
				+ this.origIndex + " -> " + this.newIndex + ", "
				+ (this.events.isEmpty() ? "[]" : "[" + this.events.stream().map(e -> e.toString()).collect(Collectors.joining(", ")) + "]") + ", "
				+ this.worksheet + ", "
				+ this.cellReferencesRange + ", "
				+ (this.excluded ? "excluded" : "visible") + ", "
				+ this.metadataContext
				+ ")";
		}
	}
}
