/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.annotation.XLIFFContextGroup;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.SubFilter;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.StartGroup;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

import static net.sf.okapi.filters.openxml.StartElementContextFactory.createStartElementContext;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isStringItemStartEvent;

class SharedStringsPart extends StyledTextPart {
    private static final String EMPTY = "";
    private static final String ROW_METADATA = "row-metadata";
    private static final String WORKSHEETS = "worksheets";
    private static final String ROWS = "rows";
    private final EncoderManager encoderManager;
    private final IFilter subfilter;
    private final SharedStrings sharedStrings;
    private int sharedStringIndex = 0;
    private IdGenerator worksheetGroupIds;
    private IdGenerator rowGroupIds;
    private StartGroup worksheetStartGroup;
    private StartGroup rowStartGroup;
    private ContextTypes contextTypes;
    private List<XLIFFContextGroup.Context> groupContexts;

    SharedStringsPart(Document.General generalDocument, ZipEntry entry, StyleDefinitions styleDefinitions,
                      StyleOptimisation styleOptimisation, EncoderManager encoderManager, IFilter subfilter, SharedStrings sharedStrings) {
        super(generalDocument, entry, styleDefinitions, styleOptimisation);

        this.encoderManager = encoderManager;
        this.subfilter = subfilter;
        this.sharedStrings = sharedStrings;
    }

    /**
     * Opens this part and performs any initial processing.  Returns the
     * first event for this part.  In this case, it's a START_SUBDOCUMENT
     * event.
     *
     * @return Event
     * @throws IOException
     * @throws XMLStreamException
     */
    @Override
    public Event open() throws IOException, XMLStreamException {
        this.documentId = this.generalDocument.documentId();
        this.subDocumentId = this.generalDocument.nextSubDocumentId();
        this.sourceLocale = this.generalDocument.sourceLocale();
        this.worksheetGroupIds = new IdGenerator(WORKSHEETS, IdGenerator.START_GROUP);

        SharedStringsDenormalizer deno = new SharedStringsDenormalizer(generalDocument.eventFactory(), sharedStrings);
        XMLEventReader reader = generalDocument.inputFactory().createXMLEventReader(
                new InputStreamReader(generalDocument.inputStreamFor(entry), StandardCharsets.UTF_8));
        final File rewrittenStringsTable = File.createTempFile("sharedStrings", ".xml");
        XMLEventWriter writer = generalDocument.outputFactory().createXMLEventWriter(
                new OutputStreamWriter(new FileOutputStream(rewrittenStringsTable), StandardCharsets.UTF_8));
        deno.process(reader, writer);

        InputStream is = new BufferedInputStream(new FileInputStream(rewrittenStringsTable));

        this.eventReader = this.generalDocument.inputFactory().createXMLEventReader(is);
        try {
            process();
        } finally {
            if (eventReader != null) {
                eventReader.close();
            }
            rewrittenStringsTable.delete();
        }
        return createStartSubDocumentEvent(documentId, subDocumentId);
    }

    private void process() throws XMLStreamException {
        while (eventReader.hasNext()) {
            final XMLEvent e = eventReader.nextEvent();
            if (!isStringItemStartEvent(e)) {
                addEventToDocumentPart(e);
                continue;
            }
            flushDocumentPart();
            if (this.sharedStrings.worksheetStartsAt(this.sharedStringIndex)) {
                endRowGroup();
                endWorksheetGroup();
                initRowGroupIds();
                startWorksheetGroup();
                startRowGroup();
            } else if (this.sharedStrings.rowStartsAt(this.sharedStringIndex)) {
                endRowGroup();
                if (!rowGroupIdsInitialised()) {
                    initRowGroupIds();
                }
                startRowGroup();
            }
            StartElementContext startElementContext = createStartElementContext(
                e.asStartElement(),
                eventReader,
                generalDocument.eventFactory(),
                this.generalDocument.conditionalParameters()
            );
            StringItem stringItem = new StringItemParser(startElementContext, nestedBlockId, styleDefinitions, styleOptimisation).parse();
            stringItem.optimiseStyles();
            final MetadataContext metadataContext = this.sharedStrings.metadataContextAt(this.sharedStringIndex);
            if (MetadataContext.ROW_AND_COLUMN == metadataContext) {
                addContextTypeFrom(stringItem);
            } else if (MetadataContext.COLUMN == metadataContext) {
                addGroupContextFrom(stringItem);
            }
            if (!this.sharedStrings.visibleAt(this.sharedStringIndex)) {
                addBlockChunksToDocumentPart(stringItem.getChunks());
                flushDocumentPart();
                this.sharedStringIndex++;
                continue;
            }
            final List<ITextUnit> textUnits = new StringItemTextUnitMapper(
                this.textUnitId,
                this.generalDocument.eventFactory(),
                stringItem,
                sharedStrings.worksheetAt(sharedStringIndex),
                sharedStrings.cellReferencesRangeAt(sharedStringIndex).first()
            ).map();
            if (textUnits.isEmpty()) {
                addBlockChunksToDocumentPart(stringItem.getChunks());
                flushDocumentPart();
            } else {
                if (subfilter != null && !stringItem.isStyled()) {
                    addSubfilteredEvents(textUnits);
                } else {
                    addTextUnitEvents(textUnits);
                }
            }
            sharedStringIndex++;
        }
        endRowGroup();
        endWorksheetGroup();
        flushDocumentPart();
        filterEvents.add(new Event(EventType.END_SUBDOCUMENT, new Ending(subDocumentId)));
        filterEventIterator = filterEvents.iterator();
    }

    private boolean rowGroupIdsInitialised() {
        return null != this.rowGroupIds;
    }

    private void initRowGroupIds() {
        final String worksheet = this.sharedStrings.worksheetAt(this.sharedStringIndex);
        this.rowGroupIds = new IdGenerator(worksheet.concat(ROWS), IdGenerator.START_GROUP);
    }

    private boolean worksheetGroupInitialised() {
        return null != this.worksheetStartGroup;
    }

    private void startWorksheetGroup() {
        this.contextTypes = new ContextTypes(
            this.generalDocument.conditionalParameters().worksheetConfigurations().metadataColumnsFor(
                this.sharedStrings.worksheetAt(this.sharedStringIndex)
            )
        );
        this.worksheetStartGroup = new StartGroup(EMPTY, this.worksheetGroupIds.createId());
        this.worksheetStartGroup.setName(this.sharedStrings.worksheetAt(this.sharedStringIndex));
        this.filterEvents.add(
            new Event(
                EventType.START_GROUP,
                this.worksheetStartGroup
            )
        );
    }

    private void endWorksheetGroup() {
        if (worksheetGroupInitialised()) {
            this.filterEvents.add(
                new Event(
                    EventType.END_GROUP,
                    new Ending(this.worksheetStartGroup.getId())
                )
            );
            this.worksheetStartGroup = null;
        }
    }

    boolean rowGroupInitialised() {
        return null != this.rowStartGroup;
    }

    private void startRowGroup() {
        this.groupContexts = new LinkedList<>();
        this.rowStartGroup = new StartGroup(EMPTY, this.rowGroupIds.createId());
        this.rowStartGroup.setName(
            String.valueOf(
                this.sharedStrings.cellReferencesRangeAt(this.sharedStringIndex).first().row()
            )
        );
        this.filterEvents.add(
            new Event(
                EventType.START_GROUP,
                this.rowStartGroup
            )
        );
    }

    private void endRowGroup() {
        if (rowGroupInitialised()) {
            if (!this.groupContexts.isEmpty()) {
                this.rowStartGroup.setAnnotation(
                    new XLIFFContextGroup(
                        this.encoderManager,
                        ROW_METADATA,
                        null,
                        null,
                        this.groupContexts
                    )
                );
            }
            this.filterEvents.add(
                new Event(
                    EventType.END_GROUP,
                    new Ending(this.rowStartGroup.getId())
                )
            );
            this.rowStartGroup = null;
        }
    }

    private void addContextTypeFrom(final StringItem stringItem) {
        this.contextTypes.add(this.sharedStrings.cellReferencesRangeAt(this.sharedStringIndex), stringItem.text());
    }

    private void addGroupContextFrom(final StringItem stringItem) {
        final XLIFFContextGroup.Context context = new XLIFFContextGroup.Context(
            this.encoderManager,
            this.contextTypes.valueFor(this.sharedStrings.cellReferencesRangeAt(this.sharedStringIndex)),
            null,
            null
        );
        context.value(stringItem.text());
        this.groupContexts.add(context);
    }

    private void addTextUnitEvents(final List<ITextUnit> textUnits) {
        for (final ITextUnit tu : textUnits) {
            filterEvents.add(new Event(EventType.TEXT_UNIT, tu));
        }
    }

    private void addSubfilteredEvents(final List<ITextUnit> textUnits) {
        int subfilterIndex = 0;
        for (final ITextUnit tu : textUnits) {
            try (final SubFilter sf = new SubFilter(subfilter, encoderManager.getEncoder(), ++subfilterIndex, tu.getId(), tu.getName())) {
                filterEvents.addAll(sf.getEvents(new RawDocument(tu.getSource().getFirstContent().getText(), sourceLocale)));
                filterEvents.add(sf.createRefEvent(tu));
            }
        }
    }

    private static class ContextTypes {
        private static final String DELIMITER = ";";
        private final Set<String> metadataColumns;
        private final Map<CellReferencesRange, String> values;

        private ContextTypes(final Set<String> metadataColumns) {
            this(
                metadataColumns,
                new LinkedHashMap<>()
            );
        }

        private ContextTypes(
            final Set<String> metadataColumns,
            final Map<CellReferencesRange, String> values
        ) {
            this.metadataColumns = metadataColumns;
            this.values = values;
        }

        void add(final CellReferencesRange cellReferencesRange, final String value) {
            this.values.put(cellReferencesRange, value);
        }

        String valueFor(final CellReferencesRange cellReferencesRange) {
            final String value = this.values.entrySet().stream()
                .filter(e -> e.getKey().anyMatch(Collections.emptySet(), cellReferencesRange.columns()))
                .map(e -> e.getValue())
                .collect(Collectors.joining(DELIMITER));
            return value.isEmpty()
                ? cellReferencesRange.columnMatches(this.metadataColumns).stream()
                    .collect(Collectors.joining(DELIMITER))
                : value;
        }
    }
}
