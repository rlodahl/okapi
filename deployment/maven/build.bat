@echo off

setlocal enabledelayedexpansion
goto mainscript

rem ===== Helper functions =====

:showSectionTitle
  echo.
  echo =============================================
  echo %*
  echo =============================================
  echo.
goto:eof

:showHelp
  call:showSectionTitle Help
  echo Unknown option "%*"
  echo.
  echo Usage: build one_or_more_options
  echo.
  echo Options for mvn install:
  echo   js : -P with_javadoc,with_sources with mvn install
  echo   nt : -DskipITs -DskipTests with mvn install
  echo   mt : -T2C with mvn install
  echo Build options:
  echo   bo : Build okapi only, then exit
  echo   ba : Build okapi and applications, then exit
  echo   ai : Build okapi, applications, integration test applications, then exit
  echo   fi : Build okapi, applications, full integration test, then exit
  echo.
  echo The mvn install options "accumulate"
  echo If more than one build options is specified (bo, ba, ai, fi), the last one wins
  echo.
  echo Example: build mt js ba
  echo   Will build Okapi proper (mvn install) with -T2C -P with_javadoc,with_sources
  echo   Will build the applications
  echo   And will end
  echo.
  pause
goto:eof

:setStepParameters
  set OKAPI_BUILD_OKAPI=%1
  set OKAPI_BUILD_APPS=%2
  set OKAPI_IT_APPS=%3
  set OKAPI_IT_ALL=%4
goto:eof

:defaultParameters
  set OKAPI_BUILD_TYPE=
  set OKAPI_BUILD_PARALLEL=
  call:setStepParameters true true true false
goto:eof

:parseArgument
  if "%1"=="js" (
    set OKAPI_BUILD_TYPE=%OKAPI_BUILD_TYPE% -P with_javadoc,with_sources
    goto:eof
  )
  if "%1"=="nt" (
    set OKAPI_BUILD_TYPE=%OKAPI_BUILD_TYPE% -DskipITs -DskipTests
    goto:eof
  )
  if "%1"=="mt" (
    set OKAPI_BUILD_PARALLEL=-T2C
    goto:eof
  )
  if "%1"=="8" (
    set MAVEN_OPTS=-Dmaven.compiler.source=1.8 -Dmaven.compiler.target=1.8
  )
  if "%1"=="11" (
    set MAVEN_OPTS=-Dmaven.compiler.source=11 -Dmaven.compiler.target=11
  )
  if "%1"=="bo" (
    call:setStepParameters true false false false
    goto:eof
  )
  if "%1"=="ba" (
    call:setStepParameters true true false false
    goto:eof
  )
  if "%1"=="ai" (
    call:setStepParameters true true true false
    goto:eof
  )
  if "%1"=="fi"  (
    call:setStepParameters true true false true
    goto:eof
  )
  call:showHelp %*
exit

# ===== Main script =====

:mainscript

if not exist ".\superpom" cd ..
if not exist ".\superpom" cd ..

call:defaultParameters
for %%x in (%*) do call:parseArgument %%x

if "%OKAPI_BUILD_OKAPI%"=="true" (
  call:showSectionTitle Build Okapi
  rem The javacc maven plugin does not work well when multithreaded.
  rem This is a workaround, we generate sources first, then build parallel.
  call mvn clean generate-sources %MAVEN_CLI_OPTS% %OKAPI_BUILD_TYPE%
  if ERRORLEVEL 1 goto error
  call mvn install %OKAPI_BUILD_PARALLEL% %MAVEN_CLI_OPTS% %OKAPI_BUILD_TYPE%
  if ERRORLEVEL 1 goto error
)

if "%OKAPI_BUILD_APPS%"=="true" (
  call:showSectionTitle Build Applications
  call mvn dependency:resolve -f okapi-ui/swt/core-ui/pom.xml -PWIN_SWT -PWIN_64_SWT -PCOCOA_64_SWT -PCOCOA_AARCH64_SWT -PLinux_x86_swt -PLinux_x86_64_swt
  if ERRORLEVEL 1 goto error
  call ant clean -f deployment/maven
  if ERRORLEVEL 1 goto error
  call ant -f deployment/maven
  if ERRORLEVEL 1 goto error
)

if "%OKAPI_IT_APPS%"=="true" (
  call:showSectionTitle Integration tests, applications only
  call mvn clean verify %MAVEN_CLI_OPTS% -f integration-tests/applications -P integration
  if ERRORLEVEL 1 goto error
)

if "%OKAPI_IT_ALL%"=="true" (
  call:showSectionTitle Integration tests, everything
  call mvn clean verify %MAVEN_CLI_OPTS% -f integration-tests -P integration
  if ERRORLEVEL 1 goto error
)

:success
call:showSectionTitle SUCCESS! Press Enter to continue.
pause
goto :eof

:error
call:showSectionTitle ERROR! Press Enter to continue.
pause
goto :eof
