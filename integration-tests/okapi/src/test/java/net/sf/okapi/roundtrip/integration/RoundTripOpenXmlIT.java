package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.openxml.OpenXMLFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripOpenXmlIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_openxml";
	private static final String DIR_NAME = "/openxml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".docx", ".pptx", ".xlsx");

	public RoundTripOpenXmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS);
	}

	@Before
	public void setUp() throws Exception {
		filter = new OpenXMLFilter();
		addKnownFailingFile("Conference_Talk.pptx");
		addKnownFailingFile("big.docx");
		addKnownFailingFile("TestDako2.docx");
		addKnownFailingFile("offce_2013_big.docx");
		addKnownFailingFile("DiamondClarity_4Cs Education Source.docx");
	}

	@After
	public void tearDown() throws Exception {
		filter.close();
	}

	@Test
	public void openXmlFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
