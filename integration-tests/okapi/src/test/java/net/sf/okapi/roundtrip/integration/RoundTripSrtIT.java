package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.XmlOrTextRoundTripIT;
import net.sf.okapi.filters.regex.RegexFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripSrtIT extends XmlOrTextRoundTripIT {
	private static final String CONFIG_ID = "okf_regex-srt";
	private static final String DIR_NAME = "/srt/";
	private static final List<String> EXTENSIONS = Arrays.asList(".srt");

	public RoundTripSrtIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS);
	}

	@Before
	public void setUp() throws Exception {
		filter = new RegexFilter();
	}

	@After
	public void tearDown() throws Exception {
		filter.close();
	}

	@Test
	public void srtFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(false, new FileComparator.Utf8FilePerLineComparator());
	}
}
