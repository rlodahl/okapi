package net.sf.okapi.conversion.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.integration.ConversionCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.tmx.TmxFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class ConvertFromTmxIT extends ConversionCompareIT {
	private static final String CONFIG_ID = "okf_tmx";
	private static final String DIR_NAME = "/tmx/";
	private static final List<String> EXTENSIONS = Arrays.asList(".tmx");
	final static FileLocation root = FileLocation.fromClass(ConvertFromTmxIT.class);

	public ConvertFromTmxIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS);
	}

	@Before
	public void setUp() throws Exception {
		filter = new TmxFilter();
	}

	@After
	public void tearDown() throws Exception {
		filter.close();
	}

	//@Ignore("Debug a single file")
	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/tmx/Paragraph_TM.tmx").asFile();
		runTest(true, file, null, null, new FileComparator.EventComparatorTextUnitOnly());
	}

	@Test
	public void tmxFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(true, new FileComparator.EventComparatorTextUnitOnly());
	}
}
